import React from "react"
import { Footer, FooterTab, Button, Text } from "native-base"
import { StyleSheet } from "react-native"
import { purple } from "../../styles/colors"

export default props => {
  return (
    <Footer>
      <FooterTab style={styles.footerTab}>
        {props.tabs.map(tab => (
          <Button
            style={{
              backgroundColor: purple,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={() => props.onActiveFooterTabChange(tab.tabNumber)}
            active={props.activeTab === tab.tabNumber}
            key={tab.tabNumber}>
            <Text style={[styles.text]}>{tab.title}</Text>
          </Button>
        ))}
      </FooterTab>
    </Footer>
  )
}

const styles = StyleSheet.create({
  footerTab: {
    flexDirection: "row-reverse",
    backgroundColor: purple,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontFamily: "Estedad-Medium",
    marginTop: 8,
    textAlign: "center",
  },
})
