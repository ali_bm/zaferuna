import axios from './axios';

export default {
  async get() {
    return await axios.get('/trade-flower/');
  },
  async getOne(id) {
    return await axios.get(`/trade-flower/${id}/`);
  },
  async add(data) {
    return await axios.post('/trade-flower/', data);
  },
  async update(data) {
    return await axios.put(`/trade-flower/${data.id}/`, data);
  },
  async remove(id) {
    return await axios.delete(`/trade-flower/${id}/`);
  },
};
