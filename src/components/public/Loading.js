import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Spinner} from 'native-base';

const Loading = (props) => {
  return (
      <View style={ [styles.container, props.style] }>
        <Spinner color={ props.color } />
        { props.children }
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Loading;
