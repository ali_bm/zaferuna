import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

function Dashboard () {
  return (
    <View style={ styles.container }>
      <Text style={ styles.text }>Dashboard</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontWeight: 'bold',
    fontSize: 32,
  },
})

export default Dashboard
