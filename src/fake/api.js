import helper from './helper'

const api = {
  purchases: [],
  sells: [],
  customers: [],
  camps: [],
  async getAllPurchases () {
    this.purchases = helper.makePurchasesItems()
    return Promise.resolve({
      items: this.purchases,
      success: true,
      error: null,
    })
  },
  async addPurchase (data) {
    data.id = this.purchases.length + 1
    this.purchases.push(data)
  },
  async getAllSells () {
    this.sells = helper.makeSellsItems()
    return Promise.resolve({
      items: this.sells,
      success: true,
      error: null,
    })
  },
  async addSell (data) {
    data.id = this.sells.length + 1
    this.sells.push(data)
    console.log(this.sells)
  },
  async getAllCustomers () {
    this.customers = helper.makeCustomerItems()
    return Promise.resolve({
      items: this.customers,
      success: true,
      error: null,
    })
  },
  async getCamps() {
    this.camps = helper.makeCampItems()
  }
}

export default api
