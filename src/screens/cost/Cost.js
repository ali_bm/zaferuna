import React, { useState, useEffect } from "react"
import { View, StyleSheet, Text } from "react-native"
import InputBox from "../../components/public/InputBox"
import { lightgreen, red, purple } from "../../styles/colors"
import StringUtils from "../../utils/StringUtils"
import Loading from "../../components/public/Loading"
import { Button } from "native-base"
import api from "../../api/api"
import DateTime from "../../utils/DateTime"

const Cost = props => {
  const [editMode, setEditMode] = useState(false)
  const [loadingPage, setLoadingPage] = useState(false)
  const [editLoading, setEditLoading] = useState(false)
  const [addLoading, setAddLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const [cost, setCost] = useState({
    id: null,
    title: null,
    date: null,
    price: null,
  })

  const fetch = async () => {
    const { editMode, id } = props.route.params
    if (editMode) {
      setEditMode(editMode)
      try {
        setLoadingPage(true)
        const res = await api.cost.getOne(id)
        if (res && res.status === 200) {
          res.data.date = DateTime.toJalaali(res.data.date)
          setCost(res.data)
          setLoadingPage(false)
        } else {
          setLoadingPage(false)
        }
      } catch (e) {
        setLoadingPage(false)
      }
    }
  }

  useEffect(() => {
    fetch()
  }, [])

  const renderAddMode = () => (
    <View style={styles.addModeContainer}>
      <Button
        onPress={() => add()}
        style={[styles.button, styles.addCampButton]}>
        {addLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ثبت هزینه</Text>
        )}
      </Button>
    </View>
  )

  const renderEditMode = () => (
    <View style={styles.editModeContainer}>
      <Button
        onPress={() => update()}
        style={[styles.button, styles.editCampButton]}>
        {editLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ثبت هزینه</Text>
        )}
      </Button>
      <Button
        onPress={() => remove()}
        style={[styles.button, styles.deleteCampButton]}>
        {deleteLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>حذف</Text>
        )}
      </Button>
    </View>
  )

  const onInputTextChange = (text, state) => {
    setCost({ ...cost, [state]: text })
  }

  const isFormValid = () => {
    return !(cost.title === null || cost.price === null || cost.date === null)
  }

  const add = async () => {
    if (isFormValid()) {
      let { title, price, date } = cost
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      const data = { title, price, date }
      try {
        setAddLoading(true)
        const res = await api.cost.add(data)
        if (res.status === 201 && res.data) {
          console.log(res.data)
          setAddLoading(false)
        }
      } catch (e) {
        console.log(e)
        setAddLoading(false)
      }
    } else {
      // wrong input notification
      console.log("wrong input")
    }
  }

  const update = async () => {
    if (isFormValid()) {
      let { id, date, title, price } = cost
      price = StringUtils.toEnglishDigits(price)
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      const data = { id, price, date, title }
      try {
        setEditLoading(true)
        const res = await api.cost.update(data)
        if (res) {
          console.log(res.data)
          setEditLoading(false)
        }
      } catch (e) {
        console.log(e)
        setEditLoading(false)
      }
    } else {
      // invalid input
    }
  }

  const remove = async () => {
    try {
      setDeleteLoading(true)
      const res = await api.cost.remove(cost.id)
      if (res) {
        console.log(res.data)
        setDeleteLoading(false)
      }
    } catch (e) {
      console.log(e)
      setDeleteLoading(false)
    }
  }

  return (
    <View style={styles.container}>
      {loadingPage ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <>
          <InputBox
            label="عنوان"
            value={cost.title}
            onChangeText={text => onInputTextChange(text, "title")}
          />
          <InputBox
            label="تاریخ"
            value={cost.date}
            onChangeText={text => onInputTextChange(text, "date")}
          />
          <InputBox
            label="مبلغ"
            value={cost.price}
            onChangeText={text => onInputTextChange(text, "price")}
            keyboardType="numeric"
          />
          <View>{editMode ? renderEditMode() : renderAddMode()}</View>
        </>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 5,
  },
  addModeContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  editModeContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    marginTop: 20,
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  addCampButton: {
    width: 100,
    marginTop: 20,
    backgroundColor: purple,
  },
  editCampButton: {
    backgroundColor: lightgreen,
    width: 45 + "%",
  },
  deleteCampButton: {
    backgroundColor: red,
    width: 45 + "%",
  },
  checkBox: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 10,
    marginTop: 10,
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

export default Cost
