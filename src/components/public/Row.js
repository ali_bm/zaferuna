import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'

const Row = (props) => {
  return (
    <TouchableOpacity style={ [styles.container, props.customStyle] }
                      onPress={ () => props.onPress() }>
      { props.children }
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row-reverse',
    height: 50,
    marginHorizontal: 5,
    marginVertical: 0.5,
  },
})

export default Row
