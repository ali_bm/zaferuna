import React from "react";
import { View, ScrollView, StyleSheet, Text, Button } from "react-native";
import HomeItem from "../components/home/HomeItem";
import Dashboard from "../components/home/Dashboard";
import * as colors from "../styles/colors";

const homeItems = [
  {
    title: "خرید و فروش",
    source: "",
    backgroundColor: colors.orange,
    scrName: "BuyAndSell",
  },
  {
    title: "پرداختی و دریافتی‌ها",
    source: "",
    backgroundColor: colors.red,
    scrName: "PaidAndReceives",
  },
  {
    title: "حساب‌ها",
    source: "",
    backgroundColor: colors.cyan,
    scrName: "Checks",
  },
  {
    title: "پایگاه‌ها",
    source: "",
    backgroundColor: colors.purple,
    scrName: "Camps",
  },
  {
    title: "هزینه‌های متفرقه",
    source: "",
    backgroundColor: colors.lightgreen,
    scrName: "ExtraExpenses",
  },
  {
    title: "پخش گل",
    source: "",
    backgroundColor: colors.light,
    scrName: "AssignFlowers",
  },
  {
    title: "مشتری‌ها",
    source: "",
    backgroundColor: colors.pink,
    scrName: "Customers",
  },
  {
    title: "آمار‌ها",
    source: "",
    backgroundColor: colors.dark,
    scrName: "Statistics",
  },
];

function Home(props) {
  function navigateToScreen(screenName) {
    props.navigation.navigate(screenName);
  }

  return (
    <View style={{ flex: 1 }}>
      <Dashboard />
      <ScrollView contentContainerStyle={styles.container}>
        {homeItems.map((i, idx) => (
          <HomeItem
            title={i.title}
            backgroundColor={i.backgroundColor}
            key={idx}
            idx={idx}
            scrName={i.scrName}
            navigate={scrName => navigateToScreen(scrName)}
          />
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexWrap: "wrap",
    alignContent: "space-between",
    flexDirection: "row-reverse",
  },
});

export default Home;
