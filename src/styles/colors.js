const orange = '#f0932b'
const red = '#eb4d4b'
const cyan = '#22a6b3'
const purple = '#686de0'
const lightgreen = '#6ab04c'
const pink = '#ff7979'
const dark = '#535c68'
const light = '#95afc0'

export { orange, red, cyan, purple, lightgreen, pink, dark, light }
