import React, { useState, useEffect } from "react"
import { View, StyleSheet, Text } from "react-native"
import { Button } from "native-base"
import { purple, lightgreen, red } from "../../styles/colors"
import InputBox from "../../components/public/InputBox"
import Select from "../../components/public/Select"
import { connect } from "react-redux"
import api from "../../api/api"
import StringUtils from "../../utils/StringUtils"
import Loading from "../../components/public/Loading"
import DateTime from "../../utils/DateTime"

const AssignFlower = props => {
  const [assignFlowerInformation, setAssignFlowerInformation] = useState({
    id: null,
    date: null,
    volume: null,
    fee: null,
    base: null,
    HeadRootVolume: null,
    baseName: null,
    DriedHeadRootVolume: null,
  })
  const [editLoading, setEditLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const [addLoading, setAddLoading] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [loadingPage, setLoadingPage] = useState(false)

  useEffect(() => {
    const get = async () => {
      const { mode, id } = props.route.params
      if (mode) {
        setEditMode(mode)
        try {
          setLoadingPage(true)
          const res = await api.assignFlower.getOne(id)
          if (res && res.status === 200) {
            res.data.date = DateTime.toJalaali(res.data.date)
            setAssignFlowerInformation(res.data)
            setLoadingPage(false)
          }
        } catch (e) {
          console.log(e)
          setLoadingPage(false)
        }
      }
    }
    get()
  }, [])

  const isFormValid = () => {
    return !(
      assignFlowerInformation.base === null ||
      assignFlowerInformation.HeadRootVolume === null ||
      assignFlowerInformation.volume === null ||
      assignFlowerInformation.fee === null ||
      assignFlowerInformation.date === null
    )
  }

  const update = async () => {
    if (isFormValid()) {
      let {
        id,
        date,
        volume,
        base,
        fee,
        HeadRootVolume,
        DriedHeadRootVolume,
      } = assignFlowerInformation
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      volume = StringUtils.toEnglishDigits(volume)
      fee = StringUtils.toEnglishDigits(fee)
      HeadRootVolume = StringUtils.toEnglishDigits(HeadRootVolume)
      DriedHeadRootVolume = DriedHeadRootVolume || null
      const data = {
        id,
        date,
        volume,
        base,
        fee,
        HeadRootVolume,
        DriedHeadRootVolume,
      }
      console.log(data)
      try {
        setEditLoading(true)
        const res = await api.assignFlower.update(data)
        console.log(res.data)
        if (res) {
          // okay back to main page
          setEditLoading(false)
        } else {
          // went wrong
          setEditLoading(false)
        }
      } catch (e) {
        console.log(e)
        setEditLoading(false)
      }
    } else {
      console.log("Assign Flower page on edit: invalid input")
    }
  }

  const add = async () => {
    if (isFormValid()) {
      let {
        date,
        volume,
        base,
        fee,
        HeadRootVolume,
        DriedHeadRootVolume,
      } = assignFlowerInformation
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      volume = StringUtils.toEnglishDigits(volume)
      fee = StringUtils.toEnglishDigits(fee)
      HeadRootVolume = StringUtils.toEnglishDigits(HeadRootVolume)
      DriedHeadRootVolume = DriedHeadRootVolume || null
      const data = {
        date,
        volume,
        base,
        fee,
        HeadRootVolume,
        DriedHeadRootVolume,
      }
      console.log(data)
      try {
        setAddLoading(true)
        console.log(data)
        const res = await api.assignFlower.add(data)
        if (res && res.status === 201) {
          setAddLoading(false)
          console.log(res.data)
        } else {
          setAddLoading(false)
        }
      } catch (e) {
        console.log(e)
        setAddLoading(false)
      }
    } else {
      // invalid input
      console.log("add: invalid input")
    }
  }

  const remove = async () => {
    setDeleteLoading(true)
    try {
      const res = await api.assignFlower.remove(assignFlowerInformation.id)
      if (res) {
        // okay back to main page
        setDeleteLoading(false)
      } else {
        // went wrong
        setDeleteLoading(false)
      }
    } catch (e) {
      console.log(e)
      setDeleteLoading(false)
    }
  }

  const onInputChangeText = (text, state) => {
    if (state === "fee") {
      text = StringUtils.toEnglishDigits(text)
    }
    setAssignFlowerInformation({ ...assignFlowerInformation, [state]: text })
  }

  const renderEditMode = () => (
    <View style={styles.editModeContainer}>
      <Button
        onPress={() => update()}
        style={[styles.button, styles.editButton]}>
        {editLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ذخیره</Text>
        )}
      </Button>
      <Button
        onPress={() => remove()}
        style={[styles.button, styles.deleteButton]}>
        {deleteLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>حذف</Text>
        )}
      </Button>
    </View>
  )

  const renderAddMode = () => (
    <View style={styles.addModeContainer}>
      <Button onPress={() => add()} style={[styles.button, styles.addButton]}>
        {addLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={[styles.buttonText]}>ثبت</Text>
        )}
      </Button>
    </View>
  )

  return (
    <View style={styles.container}>
      {loadingPage ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <>
          <Select
            onValueChange={value =>
              setAssignFlowerInformation({
                ...assignFlowerInformation,
                base: value,
              })
            }
            selectedValue={assignFlowerInformation.base}
            items={props.bases}
            label="پایگاه"
          />
          <InputBox
            underline
            onChangeText={text => onInputChangeText(text, "date")}
            label="تاریخ"
            value={assignFlowerInformation.date}
          />
          <InputBox
            underline
            onChangeText={text => onInputChangeText(text, "volume")}
            label="حجم"
            value={assignFlowerInformation.volume}
          />
          <InputBox
            underline
            onChangeText={text => onInputChangeText(text, "fee")}
            label="فی"
            value={assignFlowerInformation.fee}
          />
          <InputBox
            underline
            label="حجم سر ریشه"
            onChangeText={text => onInputChangeText(text, "HeadRootVolume")}
            value={assignFlowerInformation.HeadRootVolume}
          />
          <InputBox
            underline
            onChangeText={text =>
              onInputChangeText(text, "DriedHeadRootVolume")
            }
            label="حجم سر ریشه خشک شده"
            value={assignFlowerInformation.DriedHeadRootVolume}
          />
          {editMode ? renderEditMode() : renderAddMode()}
        </>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 5,
  },
  editModeContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    marginHorizontal: 5,
    marginTop: 20,
  },
  addModeContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 20,
  },
  editButton: {
    backgroundColor: lightgreen,
    width: 45 + "%",
  },
  deleteButton: {
    backgroundColor: red,
    width: 45 + "%",
  },
  addButton: {
    width: 100,
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
    fontSize: 15,
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

const mapStateToProps = state => {
  const { bases } = state
  return { bases }
}

export default connect(mapStateToProps)(AssignFlower)
