import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet } from "react-native"
import { Button } from "native-base"
import InputBox from "../../components/public/InputBox"
import CheckBox from "../../components/public/CheckBox"
import Loading from "../../components/public/Loading"
import Select from "../../components/public/Select"
import api from "../../api/api"
import { connect } from "react-redux"
import { lightgreen, red, purple } from "../../styles/colors"
import StringUtils from "../../utils/StringUtils"
import DateTime from "../../utils/DateTime"
import customer from "../../api/customer"

const PaidAndReceived = function (props) {
  const [customers, setCustomers] = useState([])
  const [addLoading, setAddLoading] = useState(false)
  const [editLoading, setEditLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const [pageLoading, setPageLoading] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [paidAndReceived, setPaidAndReceived] = useState({
    id: null,
    base: null,
    customer: null,
    date: "1398/09/09",
    price: "2000",
    Received: false,
    description: "jkalsjkldajl",
    isCustomer: true,
    target: null,
  })

  const fetch = async function () {
    const { receiveMode, editMode, id } = props.route.params
    setPaidAndReceived({ ...paidAndReceived, received: receiveMode })
    if (editMode) {
      setEditMode(editMode)
      try {
        setPageLoading(true)
        const res = await api.paidAndReceived.getOne(id)
        if (res && res.status === 200) {
          console.log("paid and received is: ", res.data)
          if (res.data.isCustomer) res.data.base = null
          else res.data.customer = null
          res.data.date = DateTime.toJalaali(res.data.date)
          setPaidAndReceived(res.data)
          setPageLoading(false)
        }
      } catch (e) {
        console.log("fetch paid and received error: ", e)
        setPageLoading(false)
      }
    }
  }

  const fetchCustomers = async () => {
    try {
      const res = await api.customer.get()
      if (res && res.status === 200) {
        let items = []
        for (let customer of res.data.results) {
          items.push({
            label: `${customer.name} ${customer.family}`,
            value: customer.id,
          })
        }
        setCustomers(items)
      }
    } catch (e) {
      console.log("catch error in paid and received page: ", e)
    }
  }

  useEffect(function () {
    fetchCustomers()
    fetch()
  }, [])

  const isFormValid = function () {
    return !(
      paidAndReceived.date === null ||
      paidAndReceived.price === null ||
      paidAndReceived.received === null ||
      paidAndReceived.description === null
    )
  }

  const renderAddMode = function () {
    return (
      <View style={styles.addModeContainer}>
        <Button style={[styles.button, styles.addButton]} onPress={() => add()}>
          {addLoading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={styles.buttonText}>ثبت</Text>
          )}
        </Button>
      </View>
    )
  }

  const renderEditMode = function () {
    return (
      <View style={styles.editModeContainer}>
        <Button
          style={[styles.button, styles.editButton]}
          onPress={() => update()}>
          {editLoading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={styles.buttonText}>ذخیره</Text>
          )}
        </Button>
        <Button
          style={[styles.button, styles.deleteButton]}
          onPress={() => remove()}>
          {deleteLoading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={styles.buttonText}>حذف</Text>
          )}
        </Button>
      </View>
    )
  }

  const update = async function () {
    if (isFormValid()) {
      let {
        id,
        date,
        price,
        Received,
        description,
        isCustomer,
        customer,
        base,
      } = paidAndReceived
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      price = StringUtils.toEnglishDigits(price)
      let data = { id, date, price, Received, description, isCustomer }
      if (isCustomer) {
        data.customer = customer
        delete data.base
      } else {
        data.base = base
        delete data.customer
      }
      console.log("data is: ", data)
      try {
        setEditLoading(true)
        const res = await api.paidAndReceived.update(data)
        if (res) {
          console.log("Response is: ", res.data)
          setEditLoading(false)
        } else {
          console.log("server error in add paid and received")
          setEditLoading(false)
        }
      } catch (e) {
        console.log(e)
        setEditLoading(false)
      }
    } else {
      // invlaid input
      console.log("invalid input in paid and received")
      setEditLoading(false)
    }
  }

  const remove = async function () {
    try {
      setDeleteLoading(true)
      const res = await api.paidAndReceived.remove(paidAndReceived.id)
      if (res) {
        console.log(res.status)
        setDeleteLoading(false)
      } else {
        console.log("error in paid and received remove")
        setDeleteLoading(false)
      }
    } catch (e) {
      console.log("catch run in paid and received delete: ", e)
      setDeleteLoading(false)
    }
  }

  const add = async function () {
    if (isFormValid()) {
      let {
        date,
        price,
        Received,
        description,
        isCustomer,
        customer,
        base,
      } = paidAndReceived
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      price = StringUtils.toEnglishDigits(price)
      let data = {
        date,
        price,
        Received,
        description,
        isCustomer,
      }
      if (isCustomer) {
        data.customer = customer
        delete data.base
      } else {
        data.base = base
        delete data.customer
      }
      console.log("data for add is: ", data)
      try {
        setAddLoading(true)
        const res = await api.paidAndReceived.add(data)
        if (res) {
          console.log("response add is: ", res.data)
          setAddLoading(false)
        } else {
          console.log("server error in add paid and received")
          setAddLoading(false)
        }
      } catch (e) {
        console.log("catch error in paid and received add", e)
        setAddLoading(false)
      }
    } else {
      // invlaid input
      console.log("invalid input in paid and received add")
      setEditLoading(false)
    }
  }

  const onInputChangeText = function (text, state) {
    setPaidAndReceived({ ...paidAndReceived, [state]: text })
  }

  return (
    <View style={styles.container}>
      {pageLoading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <View>
          <CheckBox
            value={paidAndReceived.isCustomer}
            onValueChange={newValue =>
              setPaidAndReceived({ ...paidAndReceived, isCustomer: newValue })
            }
            style={[styles.checkBox]}>
            <Text
              style={{
                fontFamily: "Estedad-Thin",
                color: "black",
                marginTop: -3,
              }}>
              مشتری
            </Text>
          </CheckBox>
          <Select
            onValueChange={value => {
              const state = paidAndReceived.isCustomer ? "customer" : "base"
              setPaidAndReceived({ ...paidAndReceived, [state]: value })
            }}
            selectedValue={
              paidAndReceived.isCustomer
                ? paidAndReceived.customer
                : paidAndReceived.base
            }
            items={paidAndReceived.isCustomer ? customers : props.bases}
            label={paidAndReceived.isCustomer ? "مشتری" : "پایگاه"}
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "date")}
            label="تاریخ"
            value={paidAndReceived.date}
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "price")}
            label="قیمت"
            keyboardType="numeric"
            value={paidAndReceived.price}
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "description")}
            label="توضیحات"
            value={paidAndReceived.description}
          />
          <CheckBox
            value={paidAndReceived.Received}
            onValueChange={newValue =>
              setPaidAndReceived({ ...paidAndReceived, Received: newValue })
            }
            style={[styles.checkBox]}>
            <Text
              style={{
                fontFamily: "Estedad-Thin",
                color: "black",
                marginTop: -3,
              }}>
              دریافتی
            </Text>
          </CheckBox>
          {editMode ? renderEditMode() : renderAddMode()}
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  checkBox: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 10,
    marginTop: 10,
  },
  editModeContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    marginTop: 20,
  },
  addModeContainer: {
    justifyContent: "center",
    flexDirection: "row",
  },
  buttonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  editButton: {
    width: 45 + "%",
    backgroundColor: lightgreen,
  },
  deleteButton: {
    width: 45 + "%",
    backgroundColor: red,
  },
  addButton: {
    width: 100,
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

const mapStateToProps = function (state) {
  const { bases } = state
  return { bases }
}

export default connect(mapStateToProps)(PaidAndReceived)
