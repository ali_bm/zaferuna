import "react-native-gesture-handler"
import React, { useEffect, useState } from "react"
import { StyleSheet, View, Button } from "react-native"
import { NavigationContainer } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import { getBases } from "./src/store/actions/baseAction"
import { bindActionCreators } from "redux"
import { connect } from "react-redux"

import Home from "./src/screens/Home"
import Purchases from "./src/screens/Purchases"
import PaidAndReceiveds from "./src/screens/PaidAndReceiveds"
import Customers from "./src/screens/Customers"
import Camps from "./src/screens/Camps"
import ExtraExpenses from "./src/screens/ExtraExpenses"
import Dry from "./src/screens/Dry"
import Statistics from "./src/screens/Statistics"
import Trade from "./src/screens/trade-flower/Trade"
import Customer from "./src/screens/customer/Customer"
import Camp from "./src/components/camp/Camp"
import Cost from "./src/screens/cost/Cost"
import AssignFlowers from "./src/screens/AssignFlowers"
import AssignFlower from "./src/screens/assign-flower/AssignFlower"
import PaidAndReceived from "./src/screens/paid-and-received/PaidAndReceived"
import api from "./src/api/api"
import { purple } from "./src/styles/colors"
import Login from "./src/screens/Login"
import AsyncStorage from "@react-native-community/async-storage"
import axios from "./src/api/axios"

const Stack = createStackNavigator()
const navigationOptions = {
  headerStyle: {
    backgroundColor: purple,
  },
  headerTitleStyle: {
    fontSize: 18,
    fontFamily: "Estedad-Medium",
    color: "white",
    marginBottom: 3.5,
  },
  headerTintColor: "white",
}

function App(props) {
  const [isLogedIn, setIsLogedIn] = useState(false)

  const fetchIsLogedIn = async () => {
    try {
      const value = await AsyncStorage.getItem("token")
      if (value !== null) {
        axios.defaults.headers.common["Authorization"] = "Bearer " + value
        setIsLogedIn(true)
      }
    } catch (e) {
      // error reading value
    }
  }

  useEffect(() => {
    const getBases = async () => {
      try {
        const res = await api.base.get()
        if (res.data) {
          let payload = []
          for (let d of res.data.results)
            payload.push({ label: d.name, value: d.id })
          props.getBases(payload)
        } else {
          props.getBases([])
        }
      } catch (e) {
        props.getBases([])
      }
    }
    fetchIsLogedIn()
    if (isLogedIn) {
      getBases()
    }
  }, [isLogedIn])

  const logout = async () => {
    try {
      await AsyncStorage.removeItem("token")
      setIsLogedIn(false)
    } catch (exception) {}
  }

  return (
    <View style={{ flex: 1 }}>
      {!isLogedIn ? (
        <Login userLogedIn={() => setIsLogedIn(true)} />
      ) : (
        <NavigationContainer>
          <Button onPress={() => logout()} title="Logout" />
          <View style={styles.container}>
            <Stack.Navigator>
              <Stack.Screen
                options={{ ...navigationOptions, title: "زعفرونا" }}
                name="Home">
                {props => <Home {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{
                  ...navigationOptions,
                  title: "خرید‌ها و" + " فروش‌ها",
                }}
                name="BuyAndSell">
                {props => <Purchases {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{
                  ...navigationOptions,
                  title: "پرداختی و" + " دریافتی",
                }}
                name="PaidAndReceives">
                {props => <PaidAndReceiveds {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "حساب‌ها" }}
                name="Checks">
                {props => <Customers {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "پایگاه‌ها" }}
                name="Camps">
                {props => <Camps {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "هزینه‌ها" }}
                name="ExtraExpenses">
                {props => <ExtraExpenses {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "پخش گل" }}
                name="AssignFlowers">
                {props => <AssignFlowers {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{
                  ...navigationOptions,
                  title: "خشک کردن" + " زعفران",
                }}
                name="Dry">
                {props => <Dry {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "آمار‌ها" }}
                name="Statistics">
                {props => <Statistics {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "خرید و فروش" }}
                name="Trade">
                {props => <Trade {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "مشتری‌ها" }}
                name="Customers">
                {props => <Customers {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "مشتری" }}
                name="Customer">
                {props => <Customer {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "پایگاه" }}
                name="Camp">
                {props => <Camp {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "هزینه" }}
                name="Cost">
                {props => <Cost {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "اختصاص گل" }}
                name="AssignFlower">
                {props => <AssignFlower {...props} />}
              </Stack.Screen>
              <Stack.Screen
                options={{ ...navigationOptions, title: "پرداختی و دریافتی" }}
                name="PaidAndReceived">
                {props => <PaidAndReceived {...props} />}
              </Stack.Screen>
            </Stack.Navigator>
          </View>
        </NavigationContainer>
      )}
    </View>
    // <NavigationContainer>
    //   <View style={styles.container}>
    //     <Stack.Navigator>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "زعفرونا" }}
    //         name="Home">
    //         {props => <Home {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{
    //           ...navigationOptions,
    //           title: "خرید‌ها و" + " فروش‌ها",
    //         }}
    //         name="BuyAndSell">
    //         {props => <Purchases {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{
    //           ...navigationOptions,
    //           title: "پرداختی و" + " دریافتی",
    //         }}
    //         name="PaidAndReceives">
    //         {props => <PaidAndReceiveds {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "حساب‌ها" }}
    //         name="Checks">
    //         {props => <Customers {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "پایگاه‌ها" }}
    //         name="Camps">
    //         {props => <Camps {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "هزینه‌ها" }}
    //         name="ExtraExpenses">
    //         {props => <ExtraExpenses {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "پخش گل" }}
    //         name="AssignFlowers">
    //         {props => <AssignFlowers {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{
    //           ...navigationOptions,
    //           title: "خشک کردن" + " زعفران",
    //         }}
    //         name="Dry">
    //         {props => <Dry {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "آمار‌ها" }}
    //         name="Statistics">
    //         {props => <Statistics {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "خرید و فروش" }}
    //         name="Trade">
    //         {props => <Trade {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "مشتری‌ها" }}
    //         name="Customers">
    //         {props => <Customers {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "مشتری" }}
    //         name="Customer">
    //         {props => <Customer {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "پایگاه" }}
    //         name="Camp">
    //         {props => <Camp {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "هزینه" }}
    //         name="Cost">
    //         {props => <Cost {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "اختصاص گل" }}
    //         name="AssignFlower">
    //         {props => <AssignFlower {...props} />}
    //       </Stack.Screen>
    //       <Stack.Screen
    //         options={{ ...navigationOptions, title: "پرداختی و دریافتی" }}
    //         name="PaidAndReceived">
    //         {props => <PaidAndReceived {...props} />}
    //       </Stack.Screen>
    //     </Stack.Navigator>
    //   </View>
    // </NavigationContainer>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getBases,
    },
    dispatch,
  )
}

export default connect(null, mapDispatchToProps)(App)
