import axios from './axios';

export default {
  get: async () => {
    return await axios.get('/cost/');
  },
  getOne: async id => {
    return await axios.get(`/cost/${ id }/`);
  },
  update: async data => {
    return await axios.put(`/cost/${ data.id }/`, data);
  },
  add: async data => {
    return await axios.post('/cost/', data);
  },
  remove: async id => {
    return await axios.delete(`/cost/${ id }/`);
  },
};
