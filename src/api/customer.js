import axios from "./axios"

export default {
  get: async function () {
    return await axios.get("/customers/")
  },
  getOne: async function (id) {
    return await axios.get(`/customers/${id}/`)
  },
  update: async function (data) {
    return await axios.put(`/customers/${data.id}/`, data)
  },
  add: async function (data) {
    return await axios.post("/customers/", data)
  },
  remove: async function (id) {
    return await axios.delete(`/customers/${id}/`)
  },
  getCustomerTradeFlower: async function (id) {
    return await axios.get(`/customers/${id}/trade-flower-general-book`)
  },
  getCustomerTradeMoney: async function (id) {
    return await axios.get(`/customers/${id}/trade-money-general-book`)
  },
}
