export default {
  toEnglishDigits: (str) => {
    let newValue = '';
    for (let i = 0; i < str.length; i++) {
      let charCode = str.charCodeAt(i);
      if (charCode >= 1776 && charCode <= 1785) { // For Persian digits
        charCode -= 1728;
      } else if (charCode >= 1632 && charCode <= 1641) { // For Arabic & Unix digits
        charCode -= 1584;
      }
      newValue += String.fromCharCode(charCode);
    }
    return newValue;
  },
};
