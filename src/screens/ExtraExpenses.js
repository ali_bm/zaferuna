import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet, ScrollView } from "react-native"
import Row from "../components/public/Row"
import { purple, dark } from "../styles/colors"
import api from "../api/api"
import Loading from "../components/public/Loading"
import { Button, Icon } from "native-base"
import { useIsFocused } from "@react-navigation/native"
import DateTime from "../utils/DateTime"

function ExtraExpenses(props) {
  const [costs, setCosts] = useState([])
  const [loading, setLoading] = useState(false)
  const isFocused = useIsFocused()

  const fetch = async () => {
    setLoading(true)
    const res = await api.cost.get()
    if (res && res.status === 200) {
      setCosts(res.data.results)
      setLoading(false)
    }
  }

  useEffect(() => {
    if (isFocused) {
      fetch()
    }
  }, [isFocused])

  const navigateToCostPage = (editMode, id) => {
    props.navigation.navigate("Cost", { editMode, id })
  }

  return (
    <View style={styles.container}>
      <Row onPress={() => {}} customStyle={styles.row}>
        <View style={[styles.headingCell, styles.rightCell]}>
          <Text style={styles.headingCellText}>عنوان</Text>
        </View>
        <View style={[styles.headingCell]}>
          <Text style={styles.headingCellText}>تاریخ</Text>
        </View>
        <View style={[styles.headingCell, styles.leftCell]}>
          <Text style={styles.headingCellText}>قیمت</Text>
        </View>
      </Row>
      {loading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {costs.map(cost => (
            <Row
              onPress={() => navigateToCostPage(true, cost.id)}
              key={cost.id}>
              <View style={[styles.cell, styles.rightCell]}>
                <Text style={styles.cellText}>{cost.title}</Text>
              </View>
              <View style={[styles.cell]}>
                <Text style={styles.cellText}>
                  {DateTime.toJalaali(cost.date)}
                </Text>
              </View>
              <View style={[styles.cell, styles.leftCell]}>
                <Text style={styles.cellText}>{cost.price}</Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
      <Button
        onPress={() => navigateToCostPage(false, null)}
        style={styles.addButton}>
        <Icon type="AntDesign" name="plus" />
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    justifyContent: "space-around",
    marginTop: 10,
  },
  headingCell: {
    width: 33.33333 + "%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: purple,
    borderColor: "darkgrey",
    borderWidth: 1,
  },
  headingCellText: {
    fontFamily: "Estedad-Medium",
    color: "white",
  },
  rightCell: {
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7,
  },
  leftCell: {
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
  },
  cell: {
    backgroundColor: dark,
    width: 33.33333 + "%",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  cellText: {
    color: "white",
    fontFamily: "Estedad-Thin",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
  addButton: {
    width: 60,
    height: 60,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 10,
    left: 10,
    backgroundColor: purple,
  },
})

export default ExtraExpenses
