export const getBases = payload => {
  return {
    type: "SET_BASES",
    payload,
  };
};
