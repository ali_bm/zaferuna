import React, { useState } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { Button } from 'native-base'

import InputBox from '../components/public/InputBox'

import api from '../fake/api'
import { purple } from '../styles/colors'

const AddSell = props => {
  const [sellInformation, setSellInformation] = useState({
    customerName: null,
    weight: null,
    price: null,
    date: null,
  })

  const isFormValid = () => {
    return true
    // will be updated
    // return !(customerName === '' || weight === '' || price === '' || date
    // === '')
  }

  const onAddSell = () => {
    if (isFormValid()) {
      try {
        const res = api.addSell(sellInformation)
      } catch (e) {
        console.log(e)
      }
    } else {
      // invalid input notification
      console.log('invalid input in add sell screen')
    }
  }

  const onChangeText = (state, txt) => {
    setSellInformation({
      ...sellInformation,
      [state]: txt,
    })
  }

  return (
    <View style={ styles.container }>
      <InputBox
        onChangeText={ txt => onChangeText('customerName', txt) }
        label={ 'فروشنده' }
        underline={ true }
        style={ {
          marginHorizontal: 10,
          marginTop: 10,
          borderBottomColor: '#535c68',
          borderBottomWidth: 1.5,
        } }
      />
      <InputBox
        onChangeText={ txt => onChangeText('weight', txt) }
        label={ 'وزن' }
        underline={ true }
        keyboardType="numeric"
        style={ {
          marginHorizontal: 10,
          marginTop: 10,
          borderBottomColor: '#535c68',
          borderBottomWidth: 1.5,
        } }
      />
      <InputBox
        onChangeText={ txt => onChangeText('price', txt) }
        label={ 'قیمت' }
        underline={ true }
        keyboardType="numeric"
        type="money"
        style={ {
          marginHorizontal: 10,
          marginTop: 10,
          borderBottomColor: '#535c68',
          borderBottomWidth: 1.5,
        } }
      />
      <InputBox
        onChangeText={ txt => onChangeText('date', txt) }
        label={ 'تاریخ' }
        underline={ true }
        style={ {
          marginHorizontal: 10,
          marginTop: 10,
          borderBottomColor: '#535c68',
          borderBottomWidth: 1.5,
        } }
      />
      <View style={ {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
      } }>
        <Button style={ styles.submitButton }>
          <Text style={ styles.submitButtonText }>ثبت فروش</Text>
        </Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  textInput: {},
  submitButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    height: 40,
  },
  submitButtonText: {
    fontFamily: 'Estedad-Medium',
    color: 'white',
  },
})

export default AddSell
