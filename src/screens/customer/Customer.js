import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet } from "react-native"
import InputBox from "../../components/public/InputBox"
import Loading from "../../components/public/Loading"
import { lightgreen, red, purple } from "../../styles/colors"
import api from "../../api/api"
import StringUtils from "../../utils/StringUtils"
import { Button } from "native-base"
import Footer from "../../components/public/Footer"
import CustomerCreditCards from "./CustomerCreditCards"
import CustomerTradeFlower from "./CustomerTradeFlower"
import CustomerTradeMoney from "./CustomerTradeMoney"

const footerTabs = [
  { title: "اطلاعات شخصی", tabNumber: 1 },
  { title: "خرید و فروش‌ها", tabNumber: 2 },
  { title: "پرداختی و دریافتی‌ها", tabNumber: 3 },
  { title: "کارت‌های بانکی", tabNumber: 4 },
]

export default props => {
  const [editMode, setEditMode] = useState(false)
  const [loadingPage, setLoadingPage] = useState(false)
  const [addLoading, setAddLoading] = useState(false)
  const [editLoading, setEditLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const [activeFootertTab, setActiveFooterTab] = useState(1)
  const [customer, setCustomer] = useState({
    id: null,
    name: null,
    family: null,
    phone: null,
    creditCards: [],
  })

  const fetch = async () => {
    const { editMode, id } = props.route.params
    if (editMode) {
      setEditMode(editMode)
      try {
        setLoadingPage(true)
        const res = await api.customer.getOne(id)
        if (res && res.status === 200) {
          console.log("customer is: ", res.data)
          setCustomer(res.data)
          setLoadingPage(false)
        } else {
          console.log("server error in customer fetch")
          setLoadingPage(false)
        }
      } catch (e) {
        console.log("catch error in customer fetch", e)
        setLoadingPage(false)
      }
    }
  }

  useEffect(() => {
    if (activeFootertTab === 1) {
      fetch()
    }
  }, [activeFootertTab])

  const onInputChangeText = (text, state) => {
    setCustomer({ ...customer, [state]: text })
  }

  const renderEditMode = () => (
    <View style={styles.editModeContainer}>
      <Button
        style={[styles.buttons, styles.deleteButton]}
        onPress={() => remove()}>
        {deleteLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>حذف</Text>
        )}
      </Button>
      <Button
        style={[styles.buttons, styles.editButton]}
        onPress={() => update()}>
        {editLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ذخیره</Text>
        )}
      </Button>
    </View>
  )

  const renderAddMode = () => (
    <View style={styles.addModeContainer}>
      <Button onPress={() => add()} style={styles.addButton}>
        {addLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ثبت</Text>
        )}
      </Button>
    </View>
  )

  const isFormValid = () => {
    return !(
      customer.name === null ||
      customer.family === null ||
      customer.phone === null
    )
  }

  const add = async () => {
    if (isFormValid()) {
      let { name, family, phone } = customer
      phone = StringUtils.toEnglishDigits(phone)
      const data = { name, family, phone }
      try {
        setAddLoading(true)
        const res = await api.customer.add(data)
        if (res && res.status === 201) {
          console.log(res.data)
          setAddLoading(false)
        } else {
          console.log("server error in customer add")
          setAddLoading(false)
        }
      } catch (e) {
        console.log("catch error in customer add", e)
        setAddLoading(false)
      }
    } else {
      // invalid input
    }
  }

  const update = async () => {
    if (isFormValid()) {
      let { id, name, family, phone } = customer
      phone = StringUtils.toEnglishDigits(phone)
      const data = { id, name, family, phone }
      try {
        setEditLoading(true)
        const res = await api.customer.update(data)
        if (res && res.status === 201) {
          console.log(res.data)
          setEditLoading(false)
        } else {
          console.log("server error in customer add")
          setEditLoading(false)
        }
      } catch (e) {
        console.log("catch error in customer add", e)
        setEditLoading(false)
      }
    } else {
      // invalid input
    }
  }

  const remove = async () => {
    try {
      setDeleteLoading(true)
      const res = await api.customer.remove(customer.id)
      if (res) {
        console.log(res.data)
        setDeleteLoading(false)
      } else {
        console.log("server error in customer add")
        setDeleteLoading(false)
      }
    } catch (e) {
      console.log("catch error in customer add", e)
      setDeleteLoading(false)
    }
  }

  return (
    <View style={styles.container}>
      {loadingPage ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <>
          {activeFootertTab === 1 ? (
            <View>
              <InputBox
                label={"نام"}
                underline={true}
                onChangeText={txt => onInputChangeText(txt, "name")}
                value={customer.name}
                style={styles.inputBoxStyle}
              />
              <InputBox
                label={"نام خانوادگی"}
                underline={true}
                onChangeText={txt => onInputChangeText(txt, "family")}
                value={customer.family}
                style={styles.inputBoxStyle}
              />
              <InputBox
                label={"شماره تماس"}
                underline={true}
                onChangeText={txt => onInputChangeText(txt, "phone")}
                value={customer.phone}
                style={styles.inputBoxStyle}
              />
              {editMode ? renderEditMode() : renderAddMode()}
            </View>
          ) : activeFootertTab === 2 ? (
            <CustomerTradeFlower {...props} customerId={customer.id} />
          ) : activeFootertTab === 3 ? (
            <CustomerTradeMoney {...props} customerId={customer.id} />
          ) : null}
          <View style={{ flex: 1, justifyContent: "flex-end" }}>
            {editMode ? (
              <Footer
                onActiveFooterTabChange={val => setActiveFooterTab(val)}
                tabs={footerTabs}
                activeTab={activeFootertTab}
              />
            ) : null}
          </View>
        </>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputBoxStyle: {
    padding: 7,
  },
  editModeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 10,
    marginTop: 20,
  },
  editButton: {
    backgroundColor: lightgreen,
    borderRadius: 5,
  },
  deleteButton: {
    backgroundColor: red,
    borderRadius: 5,
  },
  addButton: {
    width: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    width: 45 + "%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    fontFamily: "Estedad-Medium",
    color: "white",
    fontSize: 18,
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
  addModeContainer: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "center",
  },
})
