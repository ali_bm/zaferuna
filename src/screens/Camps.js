import React, { useState, useEffect } from "react"
import { View, Text, ScrollView, StyleSheet } from "react-native"
import { Button, Icon } from "native-base"
import { purple, dark } from "../styles/colors"
import Row from "../components/public/Row"
import api from "../api/api"
import Loading from "../components/public/Loading"
import { useIsFocused } from "@react-navigation/native"

export default props => {
  const [camps, setCamps] = useState([])
  const [loadingPage, setLoadingPage] = useState(false)
  const isFocused = useIsFocused()

  const fetch = async function () {
    try {
      setLoadingPage(true)
      const res = await api.base.get()
      if (res && res.status === 200) {
        setCamps(res.data.results)
        setLoadingPage(false)
      } else {
        setLoadingPage(false)
      }
    } catch (e) {
      setLoadingPage(false)
    }
  }

  useEffect(() => {
    if (isFocused) {
      fetch()
    }
  }, [isFocused])

  const navigateToCampPage = (editMode, id) => {
    props.navigation.navigate("Camp", { editMode, id })
  }

  return (
    <View style={styles.container}>
      <Row onPress={() => {}} customStyle={styles.row}>
        <View style={[styles.headingCell, styles.rightCell]}>
          <Text style={styles.headingCellText}>نام پایگاه</Text>
        </View>
        <View style={[styles.headingCell, styles.leftCell]}>
          <Text style={styles.headingCellText}>مسوول پایگاه</Text>
        </View>
      </Row>
      {loadingPage ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {camps.map(camp => (
            <Row
              onPress={() => navigateToCampPage(true, camp.id)}
              key={camp.id}>
              <View style={[styles.cell, styles.rightCell]}>
                <Text style={styles.cellText}>{camp.name}</Text>
              </View>
              <View style={[styles.cell, styles.leftCell]}>
                <Text style={styles.cellText}>{camp.director}</Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
      <Button
        onPress={() => navigateToCampPage(false, null)}
        style={styles.addButton}>
        <Icon type="AntDesign" name="plus" />
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    justifyContent: "space-around",
    marginTop: 10,
  },
  headingCell: {
    width: 50 + "%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: purple,
    borderColor: "darkgrey",
    borderWidth: 1,
  },
  headingCellText: {
    fontFamily: "Estedad-Medium",
    color: "white",
  },
  rightCell: {
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7,
  },
  leftCell: {
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
  },
  cell: {
    backgroundColor: dark,
    width: 50 + "%",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  cellText: {
    color: "white",
    fontFamily: "Estedad-Thin",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
  addButton: {
    width: 60,
    height: 60,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    bottom: 10,
    left: 10,
    backgroundColor: purple,
  },
})
