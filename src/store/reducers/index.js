import {combineReducers} from 'redux';
import bases from './baseReducer';

const reducers = combineReducers({
  bases,
});

export default reducers;
