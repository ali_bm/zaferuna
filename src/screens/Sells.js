import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Row from '../components/public/Row';
import DateTime from '../utils/DateTime';

// fake api import
import api from '../fake/api';

const styleConstants = {
  borderRadius: 7,
};

const Sells = props => {
  const [sells, setSells] = useState([]);

  const getSells = async() => {
    let data = await api.getAllSells();
    if(data && data.success && !data.error){
      setSells(data.items);
    }
  };

  useEffect(() => {
    const get = async() => {
      await getSells();
    };
    get();
  }, []);

  return (
      <View style={ styles.container }>
        <Row onPress={ () => {} } customStyle={ {marginTop: 10} }>
          <View
              style={ [
                styles.headingCell,
                styles.rightCell,
                {width: 35 + '%'}] }
          >
            <Text style={ styles.headingText }>نام خریدار</Text>
          </View>
          <View style={ [styles.headingCell, {width: 17.5 + '%'}] }>
            <Text style={ styles.headingText }>قیمت</Text>
          </View>
          <View style={ [styles.headingCell, {width: 17.5 + '%'}] }>
            <Text style={ styles.headingText }>وزن</Text>
          </View>
          <View
              style={ [styles.headingCell, styles.leftCell, {width: 30 + '%'}] }
          >
            <Text style={ styles.headingText }>
              تاریخ
            </Text>
          </View>
        </Row>
        <ScrollView>
          {
            sells.map(s =>
                <Row
                    key={ s.id }
                    onPress={ () => console.log(s) }
                >
                  <View
                      style={ [
                        styles.cell,
                        styles.rightCell,
                        {width: 35 + '%'}] }
                  >
                    <Text style={ styles.text }>{ s.customerName }</Text>
                  </View>
                  <View style={ [styles.cell, {width: 17.5 + '%'}] }>
                    <Text style={ styles.text }>{ s.price }</Text>
                  </View>
                  <View style={ [styles.cell, {width: 17.5 + '%'}] }>
                    <Text style={ styles.text }>{ s.weight }</Text>
                  </View>
                  <View
                      style={ [
                        styles.cell,
                        styles.leftCell,
                        {width: 30 + '%'}] }
                  >
                    <Text style={ styles.text }>
                      { DateTime.toJalaali(s.date) }
                    </Text>
                  </View>
                </Row>,
            )
          }
        </ScrollView>
        <TouchableOpacity
            style={ styles.addButton }
            onPress={ () => props.navigation.navigate('AddSell') }
        >
          <Text style={ styles.addButtonText }>Add</Text>
        </TouchableOpacity>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {},
  cell: {
    backgroundColor: '#535c68',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'darkgrey',
  },
  headingCell: {
    backgroundColor: '#686de0',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'darkgrey',
  },
  rightCell: {
    borderTopRightRadius: styleConstants.borderRadius,
    borderBottomRightRadius: styleConstants.borderRadius,
  },
  leftCell: {
    borderTopLeftRadius: styleConstants.borderRadius,
    borderBottomLeftRadius: styleConstants.borderRadius,
  },
  text: {
    fontFamily: 'Estedad-Thin',
    color: 'white',
  },
  headingText: {
    fontFamily: 'Estedad-Bold',
    color: 'white',
  },
  addButton: {
    height: 60,
    width: 60,
    backgroundColor: '#686de0',
    position: 'absolute',
    bottom: 10,
    left: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },
  addButtonText: {
    color: 'white',
    fontFamily: 'Estedad-Medium',
  },
});

export default Sells;
