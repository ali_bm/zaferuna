import React, { useState, useEffect } from "react"
import { View, StyleSheet, Text, ScrollView } from "react-native"
import { Button, Icon } from "native-base"
import Row from "../components/public/Row"
import Loading from "../components/public/Loading"
import { purple, dark } from "../styles/colors"
import api from "../api/api"
import { useIsFocused } from "@react-navigation/native"
import DateTime from "../utils/DateTime"

const AssignFlowers = props => {
  const [assignedFlowers, setAssignedFlowers] = useState([])
  const [loading, setLoading] = useState(false)
  const isFocused = useIsFocused()

  const fetch = async () => {
    setLoading(true)
    const res = await api.assignFlower.get()
    if (res && res.status === 200) {
      setAssignedFlowers(res.data.results)
      setLoading(false)
    }
  }

  const navigateToAssignFlowerPage = (mode, id) => {
    props.navigation.navigate("AssignFlower", { mode, id })
  }

  useEffect(() => {
    if (isFocused) {
      fetch()
    }
  }, [isFocused])

  return (
    <View style={styles.container}>
      <Row customStyle={styles.row}>
        <View style={[styles.cell, styles.headingCell, styles.rightCell]}>
          <Text style={styles.headingCellText}>پایگاه</Text>
        </View>
        <View style={[styles.cell, styles.headingCell]}>
          <Text style={styles.headingCellText}>تاریخ</Text>
        </View>
        <View style={[styles.cell, styles.headingCell, styles.leftCell]}>
          <Text style={styles.headingCellText}>فی</Text>
        </View>
      </Row>
      {loading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {assignedFlowers.map(assign => (
            <Row
              onPress={() => navigateToAssignFlowerPage(true, assign.id)}
              key={assign.id}>
              <View style={[styles.cell, styles.dataCell, styles.rightCell]}>
                <Text style={styles.cellText}>{assign.baseName}</Text>
              </View>
              <View style={[styles.cell, styles.dataCell]}>
                <Text style={styles.cellText}>
                  {DateTime.toJalaali(assign.date)}
                </Text>
              </View>
              <View style={[styles.cell, styles.dataCell, styles.leftCell]}>
                <Text style={styles.cellText}>{assign.fee}</Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
      <Button
        onPress={() => navigateToAssignFlowerPage(false, null)}
        style={styles.addButton}>
        <Icon name="plus" type="AntDesign" />
      </Button>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  row: {
    marginTop: 10,
  },
  headingCell: {
    backgroundColor: purple,
    width: 33.33333 + "%",
  },
  rightCell: {
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7,
  },
  leftCell: {
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
  },
  cell: {
    width: 33.3333 + "%",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  dataCell: {
    backgroundColor: dark,
  },
  headingCellText: {
    fontFamily: "Estedad-Medium",
    color: "white",
    fontSize: 18,
  },
  cellText: {
    fontFamily: "Estedad-Thin",
    color: "white",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
  addButton: {
    justifyContent: "center",
    alignItems: "center",
    width: 60,
    height: 60,
    borderRadius: 50,
    left: 10,
    bottom: 10,
    backgroundColor: purple,
  },
})

export default AssignFlowers
