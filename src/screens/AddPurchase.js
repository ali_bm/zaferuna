import React, { useState } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { Button } from 'native-base'
import InputBox from '../components/public/InputBox'
import api from '../fake/api'
import { purple } from '../styles/colors'

const AddPurchase = props => {
  const [purchaseInformation, setPurchaseInformation] = useState({
    sellerName: null,
    weight: null,
    price: null,
    date: null,
  })

  const isFormValid = () => {
    return true
    // will be updated
    // return !(sellerName === '' || weight === '' || price === '' || date === '')
  }

  const onAddPurchase = () => {
    console.log('add purchase button clicked')
    if (isFormValid()) {
      try {
        const res = api.addPurchase(purchaseInformation)
      } catch (e) {
        console.log(e)
      }
    } else {
      // invalid input notification
      console.log('invalid input in add purchase screen')
    }
  }

  const onChangeText = (state, txt) => {
    setPurchaseInformation({
      ...purchaseInformation,
      [state]: txt,
    })
  }

  return (
    <View style={ styles.container }>
      <InputBox
        onChangeText={ txt => onChangeText('sellerName', txt) }
        label={ 'فروشنده' }
        underline={ true }
        style={ {
          marginHorizontal: 10,
          marginTop: 20,
        } }
      />
      <InputBox
        onChangeText={ txt => onChangeText('weight', txt) }
        label={ 'وزن' }
        underline={ true }
        keyboardType="numeric"
        style={ {
          marginHorizontal: 10,
          marginTop: 20,
        } }
      />
      <InputBox
        onChangeText={ txt => onChangeText('price', txt) }
        label={ 'قیمت' }
        underline={ true }
        keyboardType="numeric"
        type="money"
        style={ {
          marginHorizontal: 10,
          marginTop: 10,
        } }
      />
      <InputBox
        onChangeText={ txt => onChangeText('date', txt) }
        label={ 'تاریخ' }
        underline={ true }
        style={ {
          marginHorizontal: 10,
          marginTop: 10,
        } }
      />
      <View style={ {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 20,
      } }>
        <Button
          style={ {
            alignItems: 'center',
            justifyContent: 'center',
            width: 100,
            height: 40,
          } }
          onPress={ () => onAddPurchase() }>
          <Text style={ styles.submitButtonText }>ثبت خرید</Text>
        </Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  textInput: {},
  submitButton: {},
  submitButtonText: {
    fontFamily: 'Estedad-Medium',
    color: 'white',
  },
})

export default AddPurchase
