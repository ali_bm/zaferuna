import axios from "./axios";

export default {
  get: async function () {
    return await axios.get("/trade-money/");
  },
  getOne: async function (id) {
    return await axios.get(`/trade-money/${id}/`);
  },
  update: async function (data) {
    return await axios.put(`/trade-money/${data.id}/`, data);
  },
  add: async function (data) {
    return await axios.post("/trade-money/", data);
  },
  remove: async function (id) {
    return await axios.delete(`/trade-money/${id}/`);
  },
};
