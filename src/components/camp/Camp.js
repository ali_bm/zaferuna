import React, { useState, useEffect } from "react";
import { View, StyleSheet, Text } from "react-native";
import { Button } from "native-base";
import { red, lightgreen, purple } from "../../styles/colors";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import InputBox from "../public/InputBox";
import CheckBox from "../public/CheckBox";
import Loading from "../public/Loading";
import StringUtils from "../../utils/StringUtils";
import api from "../../api/api";
import { getBases } from "../../store/actions/baseAction";

const Camp = function (props) {
  const [addLoading, setAddLoading] = useState(false);
  const [editLoading, setEditLoading] = useState(false);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const [pageLoading, setPageLoading] = useState(false);
  const [editMode, setEditMode] = useState();
  const [base, setBase] = useState({
    id: 0,
    name: null,
    director: null,
    fee: null,
    deliveryHeadRoot: false,
    permissionSell: false,
  });

  const fetch = async function () {
    const { editMode, id } = props.route.params;
    if (editMode) {
      setEditMode(editMode);
      try {
        setPageLoading(true);
        const res = await api.base.getOne(id);
        if (res && res.status === 200) {
          setBase(res.data);
          setPageLoading(false);
        } else {
          console.log("server error in base");
          setPageLoading(false);
        }
      } catch (e) {
        console.log("catch error in base fetch: ", e);
        setPageLoading(false);
      }
    }
  };

  useEffect(() => {
    fetch();
  }, []);

  const renderAddMode = () => (
    <View style={styles.addModeContainer}>
      <Button
        onPress={() => add()}
        style={[styles.button, styles.addCampButton]}>
        {addLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ثبت پایگاه</Text>
        )}
      </Button>
    </View>
  );

  const renderEditMode = () => (
    <View style={styles.editModeContainer}>
      <Button
        onPress={() => update()}
        style={[styles.button, styles.editCampButton]}>
        {editLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>ذخیره</Text>
        )}
      </Button>
      <Button
        onPress={() => remove()}
        style={[styles.button, styles.deleteCampButton]}>
        {deleteLoading ? (
          <Loading style={{ marginTop: 40 }} color="white" />
        ) : (
          <Text style={styles.buttonText}>حذف</Text>
        )}
      </Button>
    </View>
  );

  const onInputTextChange = (text, state) => {
    setBase({ ...base, [state]: text });
  };

  const isFormValid = function () {
    return !(
      base.name === null ||
      base.director === null ||
      base.fee === null ||
      base.deliveryHeadRoot === null ||
      base.permissionSell === null
    );
  };

  const add = async function () {
    if (isFormValid()) {
      let { name, director, fee, deliveryHeadRoot, permissionSell } = base;
      fee = StringUtils.toEnglishDigits(fee);
      const data = { name, director, fee, deliveryHeadRoot, permissionSell };
      try {
        setAddLoading(true);
        const res = await api.base.add(data);
        if (res && res.status === 201) {
          console.log(res.data);
          setAddLoading(false);
        } else {
          console.log("server error in base add");
          setAddLoading(false);
        }
      } catch (e) {
        console.log(e);
        setAddLoading(false);
      }
    } else {
      // invalid input
    }
  };

  const update = async function () {
    if (isFormValid()) {
      let { id, name, director, fee, deliveryHeadRoot, permissionSell } = base;
      fee = StringUtils.toEnglishDigits(fee);
      const data = {
        id,
        name,
        director,
        fee,
        deliveryHeadRoot,
        permissionSell,
      };
      try {
        setEditLoading(true);
        const res = await api.base.update(data);
        if (res) {
          console.log("edit base done", res.data);
          setEditLoading(false);
        } else {
          console.log("server error in base add");
          setEditLoading(false);
        }
      } catch (e) {
        console.log("catch error in base update", e);
        setEditLoading(false);
      }
    } else {
      // wrong input
    }
  };

  const remove = async function () {
    try {
      setDeleteLoading(true);
      const res = await api.base.remove(base.id);
      if (res) {
        console.log(res.data);
        setDeleteLoading(false);
      } else {
        setEditLoading(false);
      }
    } catch (e) {
      setDeleteLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      {pageLoading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <>
          <View>
            <InputBox
              label="نام پایگاه"
              value={base.name}
              onChangeText={text => onInputTextChange(text, "name")}
            />
            <InputBox
              label="مسوول پایگاه"
              value={base.director}
              onChangeText={text => onInputTextChange(text, "director")}
            />
            <InputBox
              label="فی سالانه"
              value={base.fee}
              onChangeText={text => onInputTextChange(text, "fee")}
              keyboardType="numeric"
            />

            <CheckBox
              value={base.permissionSell}
              onValueChange={newValue =>
                setBase({ ...base, permissionSell: newValue })
              }
              style={[styles.checkBox]}>
              <Text
                style={{
                  fontFamily: "Estedad-Medium",
                  color: "black",
                  marginTop: -3,
                }}>
                اجازه فروش دارد؟
              </Text>
            </CheckBox>
            <CheckBox
              value={base.deliveryHeadRoot}
              onValueChange={newValue =>
                setBase({
                  ...base,
                  deliveryHeadRoot: newValue,
                })
              }
              style={[styles.checkBox]}>
              <Text
                style={{
                  fontFamily: "Estedad-Medium",
                  color: "black",
                  marginTop: -3,
                }}>
                سر ریشه تحویل شده است؟
              </Text>
            </CheckBox>
          </View>
          <View>{editMode ? renderEditMode() : renderAddMode()}</View>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  addModeContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  editModeContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    marginTop: 20,
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  addCampButton: {
    width: 100,
    marginTop: 20,
    backgroundColor: purple,
  },
  editCampButton: {
    backgroundColor: lightgreen,
    width: 45 + "%",
  },
  deleteCampButton: {
    backgroundColor: red,
    width: 45 + "%",
  },
  checkBox: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 10,
    marginTop: 10,
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
});

const mapDispatchToProps = function (dispatch) {
  return bindActionCreators(
    {
      getBases,
    },
    dispatch,
  );
};

export default connect(null, mapDispatchToProps)(Camp);
