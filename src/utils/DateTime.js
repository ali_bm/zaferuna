import moment from 'jalali-moment';

export default {
  toJalaali: (date) => {
    return moment(date, 'YYYY/MM/DD').format('jYYYY/jMM/jDD');
  },
  toUTC: (date) => {
    return moment(date, 'jYYYY/jMM/jDD').format('YYYY-MM-DD');
  },
};
