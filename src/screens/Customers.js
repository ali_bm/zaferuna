import React, { useState, useEffect } from "react";
import { View, ScrollView, Text, StyleSheet } from "react-native";
import { Button, Icon } from "native-base";
import Row from "../components/public/Row";
import { purple } from "../styles/colors";
import api from "../api/api";
import { useIsFocused } from "@react-navigation/native";
import Loading from "../components/public/Loading";

const styleConstants = {
  borderRadius: 7,
};

export default props => {
  const [loadingPage, setLoadingPage] = useState(false);
  const [customers, setCustomers] = useState([]);
  const isFocused = useIsFocused();

  const getCustomerFullName = (firstName, lastName) => {
    return `${firstName} ${lastName}`;
  };

  const fetch = async () => {
    try {
      setLoadingPage(true);
      const res = await api.customer.get();
      if (res && res.status === 200) {
        console.log("customers are: ", res.data.results);
        setCustomers(res.data.results);
        setLoadingPage(false);
      }
    } catch (e) {
      console.log("catch error in fetch customers: ", e);
    }
  };

  useEffect(() => {
    if (isFocused) {
      fetch();
    }
  }, [isFocused]);

  const navigateToCustomerPage = (editMode, id) => {
    props.navigation.navigate("Customer", { editMode, id });
  };

  return (
    <View style={styles.container}>
      <Row onPress={() => {}} customStyle={{ marginTop: 10 }}>
        <View
          style={[styles.headingCell, styles.rightCell, { width: 50 + "%" }]}>
          <Text style={styles.headingText}>مشتری</Text>
        </View>
        <View
          style={[styles.headingCell, styles.leftCell, { width: 50 + "%" }]}>
          <Text style={styles.headingText}>شماره تماس</Text>
        </View>
      </Row>
      {loadingPage ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {customers.map(c => (
            <Row key={c.id} onPress={() => navigateToCustomerPage(true, c.id)}>
              <View
                style={[styles.cell, styles.rightCell, { width: 50 + "%" }]}>
                <Text style={styles.text}>
                  {getCustomerFullName(c.name, c.family)}
                </Text>
              </View>
              <View style={[styles.cell, styles.leftCell, { width: 50 + "%" }]}>
                <Text style={styles.text}>{c.phone}</Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
      <Button
        style={styles.addButton}
        onPress={() => navigateToCustomerPage(false, null)}>
        <Icon type="AntDesign" name="plus" />
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {},
  cell: {
    backgroundColor: "#535c68",
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  headingCell: {
    backgroundColor: purple,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  rightCell: {
    borderTopRightRadius: styleConstants.borderRadius,
    borderBottomRightRadius: styleConstants.borderRadius,
  },
  leftCell: {
    borderTopLeftRadius: styleConstants.borderRadius,
    borderBottomLeftRadius: styleConstants.borderRadius,
  },
  text: {
    fontFamily: "Estedad-Thin",
    color: "white",
  },
  headingText: {
    fontFamily: "Estedad-Bold",
    color: "white",
  },
  addButton: {
    height: 60,
    width: 60,
    backgroundColor: purple,
    position: "absolute",
    bottom: 10,
    left: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
  },
  addButtonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
});
