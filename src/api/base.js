import axios from "./axios";

export default {
  get: async () => {
    return await axios.get("/base/");
  },
  getOne: async function (id) {
    return await axios.get(`/base/${id}/`);
  },
  update: async data => {
    return await axios.put(`/base/${data.id}/`, data);
  },
  add: async data => {
    return await axios.post("/base/", data);
  },
  remove: async id => {
    return await axios.delete(`/base/${id}/`);
  },
};
