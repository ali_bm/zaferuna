import axios from "./axios"

export default {
  login: async function (data) {
    return await axios.post("/api/token/", data)
  },
}
