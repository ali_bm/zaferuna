import React from 'react';
import {View, StyleSheet, Picker, Text} from 'react-native';
import {Item, Label} from 'native-base';

const Select = props => {
  return (
      <View style={ [props.style] }>
        <Item style={ [styles.item] }
              floatingLabel={ props.floatingLabel }
              stackedLabel={ props.stackedLabel }
              regular={ props.regular }
              rounded={ props.rounded }
              underline={ props.underline }>
          <Label style={ styles.label }>{ props.label }:</Label>
          <Picker
              selectedValue={ props.selectedValue }
              style={ {
                height: 50,
                width: 100 + '%',
              } }
              onValueChange={ itemValue => props.onValueChange(itemValue) }
          >
            { props.items.map(
                pickerItem => <Picker.Item key={ pickerItem.value }
                                           value={ pickerItem.value }
                                           label={ pickerItem.label } />) }
          </Picker>
        </Item>
      </View>
  );
};

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row-reverse',
  },
  label: {
    fontFamily: 'Estedad-Thin',
    fontSize: 15,
  },
  text: {
    fontFamily: 'Estedad-Medium',
  },
});

export default Select;
