import axios from "axios"
import AsyncStorage from "@react-native-community/async-storage"

axios.defaults.baseURL = "https://zaferuna.aliebrahimi.me/"

export default axios
