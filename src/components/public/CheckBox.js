import React from 'react';
import {StyleSheet, CheckBox, View} from 'react-native';

export default props => {
  return (
      <View style={ [styles.container, props.style] }>
        <CheckBox value={ props.value }
                  onValueChange={ newValue => props.onValueChange(newValue) } />
        { props.children }
      </View>
  );
}

const styles = StyleSheet.create({
  container: {},
});
