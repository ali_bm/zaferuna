import axios from "./axios"

export default {
  get: async () => {
    return await axios.get("/assign-flower/")
  },
  getOne: async id => {
    return await axios.get(`/assign-flower/${id}/`)
  },
  update: async data => {
    return await axios.put(`/assign-flower/${data.id}/`, data)
  },
  add: async data => {
    return await axios.post("/assign-flower/", data)
  },
  remove: async id => {
    return axios.delete(`/assign-flower/${id}/`)
  },
}
