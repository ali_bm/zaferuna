import React, { useState, useEffect } from "react"
import { View, ScrollView, Text, StyleSheet } from "react-native"
import { Button, Icon } from "native-base"
import Row from "../components/public/Row"
import DateTime from "../utils/DateTime"
import Loading from "../components/public/Loading"
import { purple, dark, lightgreen, red } from "../styles/colors"
import { useIsFocused } from "@react-navigation/native"
import api from "../api/api"

const styleConstants = {
  borderRadius: 7,
}

function Purchases(props) {
  const [orders, setOrders] = useState([])
  const [loading, setLoading] = useState(false)
  const isFocused = useIsFocused()

  const fetch = async function () {
    setLoading(true)
    try {
      const res = await api.tradeFlower.get()
      if (res && res.status === 200) {
        setOrders(res.data.results)
        setLoading(false)
      } else {
        setLoading(false)
      }
    } catch (e) {
      setLoading(false)
      // connection error
    }
  }

  useEffect(() => {
    if (isFocused) {
      fetch()
    }
  }, [isFocused])

  const navigateToTradePage = function (buyMode, editMode, id) {
    props.navigation.navigate("Trade", { buyMode, editMode, id })
  }

  return (
    <View style={styles.container}>
      <Row onPress={() => {}} customStyle={{ marginTop: 10 }}>
        <View
          style={[styles.headingCell, styles.rightCell, { width: 35 + "%" }]}>
          <Text style={styles.headingText}>نام</Text>
        </View>
        <View style={[styles.headingCell, { width: 17.5 + "%" }]}>
          <Text style={styles.headingText}>قیمت</Text>
        </View>
        <View style={[styles.headingCell, { width: 17.5 + "%" }]}>
          <Text style={styles.headingText}>وزن</Text>
        </View>
        <View
          style={[styles.headingCell, styles.leftCell, { width: 30 + "%" }]}>
          <Text style={styles.headingText}>تاریخ</Text>
        </View>
      </Row>
      {loading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {orders.map(order => (
            <Row
              key={order.id}
              onPress={() => navigateToTradePage(order.buy, true, order.id)}>
              <View
                style={[
                  styles.cell,
                  styles.rightCell,
                  {
                    width: 35 + "%",
                    backgroundColor: order.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{`${order.customerName}`}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  {
                    width: 17.5 + "%",
                    backgroundColor: order.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{order.cost}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  {
                    width: 17.5 + "%",
                    backgroundColor: order.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{order.weight}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  styles.leftCell,
                  {
                    width: 30 + "%",
                    backgroundColor: order.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>
                  {DateTime.toJalaali(order.date)}
                </Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
      <View style={styles.addButtonsContainer}>
        <Button
          onPress={() => navigateToTradePage(false, false, null)}
          style={styles.addSellButton}>
          <Icon type="AntDesign" name="plus" />
        </Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {},
  cell: {
    backgroundColor: dark,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  headingCell: {
    backgroundColor: purple,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  rightCell: {
    borderTopRightRadius: styleConstants.borderRadius,
    borderBottomRightRadius: styleConstants.borderRadius,
  },
  leftCell: {
    borderTopLeftRadius: styleConstants.borderRadius,
    borderBottomLeftRadius: styleConstants.borderRadius,
  },
  text: {
    fontFamily: "Estedad-Thin",
    color: "white",
  },
  headingText: {
    fontFamily: "Estedad-Bold",
    color: "white",
  },
  addButtonsContainer: {
    position: "absolute",
    bottom: 0,
    height: 50,
    width: 100 + "%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  addBuyButton: {
    backgroundColor: lightgreen,
  },
  addSellButton: {
    backgroundColor: purple,
    borderRadius: 50,
    position: "absolute",
    width: 60,
    height: 60,
    left: 10,
    bottom: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  addButtonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

export default Purchases
