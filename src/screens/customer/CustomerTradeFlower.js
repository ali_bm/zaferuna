import React, { useEffect, useState } from "react"
import { StyleSheet, View, Text, ScrollView } from "react-native"
import Loading from "../../components/public/Loading"
import { dark, purple, lightgreen, red } from "../../styles/colors"
import Row from "../../components/public/Row"
import api from "../../api/api"
import DateTime from "../../utils/DateTime"

const styleConstants = {
  borderRadius: 7,
}

const CustomerTradeFlower = props => {
  const [loading, setLoading] = useState(false)
  const [customerTradeFlowers, setCustomerTradeFlowers] = useState([])
  const fetchCustomerTradeFlower = async () => {
    try {
      setLoading(true)
      const res = await api.customer.getCustomerTradeFlower(props.customerId)
      if (res && res.status === 200) {
        console.log("customer trade flowers are: ", res.data.results)
        setCustomerTradeFlowers(res.data.results)
        setLoading(false)
      }
    } catch (e) {
      console.log("catch error in CustomerTradeflower fetch: ", e)
    }
  }

  useEffect(() => {
    fetchCustomerTradeFlower()
  }, [])

  const navigateToTradePage = (buyMode, editMode, id) => {
    props.navigation.navigate("Trade", { buyMode, editMode, id })
  }

  return (
    <View style={styles.container}>
      <Row onPress={() => {}} customStyle={{ marginTop: 10 }}>
        <View
          style={[styles.headingCell, styles.rightCell, { width: 35 + "%" }]}>
          <Text style={styles.headingText}>نام</Text>
        </View>
        <View style={[styles.headingCell, { width: 17.5 + "%" }]}>
          <Text style={styles.headingText}>قیمت</Text>
        </View>
        <View style={[styles.headingCell, { width: 17.5 + "%" }]}>
          <Text style={styles.headingText}>وزن</Text>
        </View>
        <View
          style={[styles.headingCell, styles.leftCell, { width: 30 + "%" }]}>
          <Text style={styles.headingText}>تاریخ</Text>
        </View>
      </Row>
      {loading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {customerTradeFlowers.map(ctf => (
            <Row
              key={ctf.id}
              onPress={() => navigateToTradePage(ctf.buy, true, ctf.id)}>
              <View
                style={[
                  styles.cell,
                  styles.rightCell,
                  {
                    width: 35 + "%",
                    backgroundColor: ctf.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{ctf.customerName}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  {
                    width: 17.5 + "%",
                    backgroundColor: ctf.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{ctf.cost}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  {
                    width: 17.5 + "%",
                    backgroundColor: ctf.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{ctf.weight}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  styles.leftCell,
                  {
                    width: 30 + "%",
                    backgroundColor: ctf.buy ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{DateTime.toJalaali(ctf.date)}</Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {},
  cell: {
    backgroundColor: dark,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  headingCell: {
    backgroundColor: purple,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  rightCell: {
    borderTopRightRadius: styleConstants.borderRadius,
    borderBottomRightRadius: styleConstants.borderRadius,
  },
  leftCell: {
    borderTopLeftRadius: styleConstants.borderRadius,
    borderBottomLeftRadius: styleConstants.borderRadius,
  },
  text: {
    fontFamily: "Estedad-Thin",
    color: "white",
  },
  headingText: {
    fontFamily: "Estedad-Bold",
    color: "white",
  },
  addButtonsContainer: {
    position: "absolute",
    bottom: 0,
    height: 50,
    width: 100 + "%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  addBuyButton: {
    backgroundColor: lightgreen,
  },
  addSellButton: {
    backgroundColor: purple,
    borderRadius: 50,
    position: "absolute",
    width: 60,
    height: 60,
    left: 10,
    bottom: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  addButtonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

export default CustomerTradeFlower
