import React, { useState } from "react"
import { View, Text, StyleSheet } from "react-native"
import { Button } from "native-base"
import InputBox from "../components/public/InputBox"
import { purple } from "../styles/colors"
import api from "../api/api"
import axios from "../api/axios"
import AsyncStorage from "@react-native-community/async-storage"
import Loading from "../components/public/Loading"

const Login = props => {
  const [userName, setUserName] = useState(null)
  const [password, setPassword] = useState(null)
  const [loading, setLoading] = useState(false)

  const onUserNameChangeText = text => {
    setUserName(text)
  }

  const onPasswordChangeText = text => {
    setPassword(text)
  }

  const isFormValid = () => {
    return !(userName === null || password === null)
  }

  const login = async () => {
    if (isFormValid()) {
      try {
        setLoading(true)
        const res = await api.auth.login({ username: userName, password })
        if (res) {
          console.log(res.data)
          try {
            await AsyncStorage.setItem("token", res.data.access)
            const token = res.data.access
            axios.defaults.headers.common["Authorization"] = "Bearer " + token
            props.userLogedIn()
            setLoading(false)
          } catch (e) {
            // saving error
          }
        }
      } catch (e) {
        console.log(e)
      }
    }
  }

  return (
    <View style={styles.container}>
      <InputBox
        onChangeText={text => onUserNameChangeText(text)}
        label="نام کاربری"
        value={userName}
        style={{ width: 90 + "%" }}
        textAlign="left"
      />
      <InputBox
        onChangeText={text => onPasswordChangeText(text)}
        label="رمز عبور"
        value={password}
        style={{ width: 90 + "%", marginTop: 20 }}
        textAlign="left"
      />
      <View>
        <Button
          onPress={() => login()}
          style={{
            width: 100,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: purple,
            marginTop: 30,
          }}>
          {loading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={{ fontFamily: "Estedad-Medium", color: "white" }}>
              ورود
            </Text>
          )}
        </Button>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
})

export default Login
