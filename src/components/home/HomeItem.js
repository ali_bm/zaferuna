import React from 'react'
import { View, TouchableOpacity, Text, Image, StyleSheet } from 'react-native'

function HomeItem (props) {

  function getPadding (idx) {
    const padding = 8
    return idx % 2 === 0 ? {
      padding,
      paddingLeft: padding / 2,
      paddingVertical: padding / 2,
    } : {
      padding,
      paddingRight: padding / 2,
      paddingVertical: padding / 2,
    }
  }

  return (
    <TouchableOpacity
      style={ [styles.container, getPadding(props.idx)] }
      onPress={ () => props.navigate(props.scrName) }>
      <View
        style={ [styles.wrapper, { backgroundColor: props.backgroundColor }] }
      >
        <Image source={ props.source } style={ styles.image } />
        <Text style={ styles.text }>{ props.title }</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '50%',
  },
  wrapper: {
    width: 100 + '%',
    minHeight: 115,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
  },
  text: {
    color: 'white',
    fontFamily: 'Estedad-Medium',
    fontSize: 17,
  },
})

export default HomeItem
