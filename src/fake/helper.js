export default {
  makePurchasesItems () {
    let items = []
    for (let i = 0; i < 20; i++) {
      const item = {
        id: i + 1,
        sellerName: 'حمید معصومی نژاد',
        price: 1000,
        weight: 100.6,
        date: '2020/07/09',
      }
      items.push(item)
    }
    return items
  },
  makeSellsItems () {
    let items = []
    for (let i = 0; i < 20; i++) {
      const item = {
        id: i + 1,
        customerName: 'احمد معصومی نژاد',
        price: 8000,
        weight: 88,
        date: '2020/01/01',
      }
      items.push(item)
    }
    return items
  },
  makeCustomerItems () {
    let items = []
    for (let i = 0; i < 20; i++) {
      const item = {
        id: i + 1,
        firstName: 'علی',
        lastName: 'بیات مختاری',
        phoneNumber: '09333333333',
        creditCards: [
          {
            id: 1,
            cardNumber: '6037899056873454',
          },
          {
            id: 2,
            cardNumber: '6037899056873454',
          },
        ],
      }
      items.push(item)
    }
    return items
  },
  makeCampItems: () => {
    return []
  },
}
