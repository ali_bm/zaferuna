import React from "react"
import { StyleSheet, View } from "react-native"
import { Item, Input, Container, Content, Label } from "native-base"
import Money from "../../utils/Money"

export default props => {
  return (
    <View style={[props.style]}>
      <Item
        style={[styles.item]}
        floatingLabel={props.floatingLabel}
        stackedLabel={props.stackedLabel}
        regular={props.regular}
        rounded={props.rounded}
        underline={props.underline}>
        <Label style={styles.label}>{props.label}:</Label>
        <Input
          editable={props.editable}
          selectTextOnFocus={props.selectTextOnFocus}
          keyboardType={props.keyboardType}
          style={styles.text}
          textAlign={props.textAlign ? props.textAlign : "right"}
          onChangeText={text => props.onChangeText(text)}
          value={props.value}
        />
      </Item>
    </View>
  )
}

const styles = StyleSheet.create({
  item: {
    flexDirection: "row-reverse",
  },
  label: {
    fontFamily: "Estedad-Thin",
    fontSize: 15,
  },
  text: {
    fontFamily: "Estedad-Medium",
  },
})
