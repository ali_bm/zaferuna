import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet } from "react-native"
import { Button } from "native-base"
import InputBox from "../../components/public/InputBox"
import CheckBox from "../../components/public/CheckBox"
import Loading from "../../components/public/Loading"
import Select from "../../components/public/Select"
import api from "../../api/api"
import { connect } from "react-redux"
import { lightgreen, red, purple } from "../../styles/colors"
import StringUtils from "../../utils/StringUtils"
import DateTime from "../../utils/DateTime"

const Trade = function (props) {
  const [addLoading, setAddLoading] = useState(false)
  const [editLoading, setEditLoading] = useState(false)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const [pageLoading, setPageLoading] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [customers, setCustomers] = useState([])
  const [trade, setTrade] = useState({
    id: null,
    customer: null,
    date: null,
    fee: null,
    cost: null,
    weight: null,
    buy: null,
  })

  const fetchCustomers = async function () {
    try {
      const res = await api.customer.get()
      if (res && res.status === 200) {
        let items = []
        for (let customer of res.data.results) {
          items.push({
            label: `${customer.name} ${customer.family}`,
            value: customer.id,
          })
        }
        setCustomers(items)
      } else {
        console.log("server error in add trade")
      }
    } catch (e) {
      console.log("fetch customers error: ", e)
    }
  }

  const fetch = async function () {
    const { buyMode, editMode, id } = props.route.params
    setTrade({ ...trade, buy: buyMode })
    if (editMode) {
      setEditMode(editMode)
      try {
        setPageLoading(true)
        const res = await api.tradeFlower.getOne(id)
        if (res && res.status === 200) {
          res.data.date = DateTime.toJalaali(res.data.date)
          setTrade(res.data)
          setPageLoading(false)
        }
      } catch (e) {
        console.log("fetch trade error: ", e)
        setPageLoading(false)
      }
    }
  }

  useEffect(function () {
    fetchCustomers()
    fetch()
  }, [])

  const isFormValid = function () {
    return !(
      trade.customer === null ||
      trade.weight === null ||
      trade.fee === null ||
      trade.cost === null ||
      trade.date === null ||
      trade.buy === null
    )
  }

  const renderAddMode = function () {
    return (
      <View style={styles.addModeContainer}>
        <Button style={[styles.button, styles.addButton]} onPress={() => add()}>
          {addLoading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={styles.buttonText}>ثبت</Text>
          )}
        </Button>
      </View>
    )
  }

  const renderEditMode = function () {
    return (
      <View style={styles.editModeContainer}>
        <Button
          style={[styles.button, styles.editButton]}
          onPress={() => update()}>
          {editLoading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={styles.buttonText}>ذخیره</Text>
          )}
        </Button>
        <Button
          style={[styles.button, styles.deleteButton]}
          onPress={() => remove()}>
          {deleteLoading ? (
            <Loading style={{ marginTop: 40 }} color="white" />
          ) : (
            <Text style={styles.buttonText}>حذف</Text>
          )}
        </Button>
      </View>
    )
  }

  const update = async function () {
    if (isFormValid()) {
      let { id, fee, weight, date, customer, buy, cost } = trade
      fee = StringUtils.toEnglishDigits(fee)
      weight = StringUtils.toEnglishDigits(weight)
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      cost = StringUtils.toEnglishDigits(cost)
      const data = { id, fee, customer, weight, buy, date, cost }
      try {
        setEditLoading(true)
        const res = await api.tradeFlower.update(data)
        if (res) {
          console.log(res.data)
          setEditLoading(false)
        } else {
          console.log("server error in add trade")
          setEditLoading(false)
        }
      } catch (e) {
        console.log(e)
        setEditLoading(false)
      }
    } else {
      // invlaid input
      console.log("invalid input in trade screen")
      setEditLoading(false)
    }
  }

  const remove = async function () {
    try {
      setDeleteLoading(true)
      const res = await api.tradeFlower.remove(trade.id)
      if (res) {
        console.log(res.data)
        setDeleteLoading(false)
      } else {
        console.log("error in trade remove")
        setDeleteLoading(false)
      }
    } catch (e) {
      console.log("catch run in trade delete: ", e)
      setDeleteLoading(false)
    }
  }

  const add = async function () {
    if (isFormValid()) {
      let { fee, weight, date, customer, buy, cost } = trade
      fee = StringUtils.toEnglishDigits(fee)
      weight = StringUtils.toEnglishDigits(weight)
      date = DateTime.toUTC(StringUtils.toEnglishDigits(date)) + "T22:00:00Z"
      cost = StringUtils.toEnglishDigits(cost)
      const data = { fee, customer, weight, buy, date, cost }
      try {
        setAddLoading(true)
        const res = await api.tradeFlower.add(data)
        if (res) {
          console.log(res.data)
          setAddLoading(false)
        } else {
          console.log("server error in add trade")
          setAddLoading(false)
        }
      } catch (e) {
        console.log("catch error in trade add", e)
        setAddLoading(false)
      }
    } else {
      // invlaid input
      console.log("invalid input in trade screen add")
      setEditLoading(false)
    }
  }

  const onInputChangeText = function (text, state) {
    setTrade({ ...trade, [state]: text })
  }

  return (
    <View style={styles.container}>
      {pageLoading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <View>
          <Select
            onValueChange={value => setTrade({ ...trade, customer: value })}
            selectedValue={trade.customer}
            items={customers}
            label="نام"
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "date")}
            label="تاریخ"
            value={trade.date}
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "fee")}
            label="فی"
            value={trade.fee}
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "cost")}
            label="قیمت"
            value={trade.cost}
          />
          <InputBox
            onChangeText={text => onInputChangeText(text, "weight")}
            label="وزن"
            value={trade.weight}
          />
          <CheckBox
            value={trade.buy}
            onValueChange={newValue => setTrade({ ...trade, buy: newValue })}
            style={[styles.checkBox]}>
            <Text
              style={{
                fontFamily: "Estedad-Thin",
                color: "black",
                marginTop: -3,
              }}>
              خرید
            </Text>
          </CheckBox>
          {editMode ? renderEditMode() : renderAddMode()}
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  checkBox: {
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    marginRight: 10,
    marginTop: 10,
  },
  editModeContainer: {
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    marginTop: 20,
  },
  addModeContainer: {
    justifyContent: "center",
    flexDirection: "row",
  },
  buttonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  editButton: {
    width: 45 + "%",
    backgroundColor: lightgreen,
  },
  deleteButton: {
    width: 45 + "%",
    backgroundColor: red,
  },
  addButton: {
    width: 100,
    backgroundColor: purple,
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

const mapStateToProps = function (state) {
  const { bases } = state
  return { bases }
}

export default connect(mapStateToProps)(Trade)
