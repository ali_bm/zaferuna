import React, { useEffect, useState } from "react"
import { StyleSheet, View, Text, ScrollView } from "react-native"
import Loading from "../../components/public/Loading"
import { dark, purple, lightgreen, red } from "../../styles/colors"
import Row from "../../components/public/Row"
import api from "../../api/api"
import DateTime from "../../utils/DateTime"

const styleConstants = {
  borderRadius: 7,
}

const CustomerTradeMoney = props => {
  const [loading, setLoading] = useState(false)
  const [customerTradeMoney, setCustomerTradeMoney] = useState([])
  const fetchCustomerTradeMoney = async () => {
    try {
      setLoading(true)
      const res = await api.customer.getCustomerTradeMoney(props.customerId)
      if (res && res.status === 200) {
        console.log("customer trade flowers are: ", res.data.results)
        setCustomerTradeMoney(res.data.results)
        setLoading(false)
      }
    } catch (e) {
      console.log("catch error in CustomerTradeMoney fetch: ", e)
    }
  }

  useEffect(() => {
    fetchCustomerTradeMoney()
  }, [])

  const navigateToPaidAndReceivedPage = (buyMode, editMode, id) => {
    props.navigation.navigate("PaidAndReceived", { buyMode, editMode, id })
  }

  return (
    <View style={styles.container}>
      <Row onPress={() => {}} customStyle={{ marginTop: 10 }}>
        <View
          style={[
            styles.headingCell,
            styles.rightCell,
            { width: 33.3333 + "%" },
          ]}>
          <Text style={styles.headingText}>نام</Text>
        </View>
        <View style={[styles.headingCell, { width: 33.3333 + "%" }]}>
          <Text style={styles.headingText}>قیمت</Text>
        </View>
        <View
          style={[
            styles.headingCell,
            styles.leftCell,
            { width: 33.3333 + "%" },
          ]}>
          <Text style={styles.headingText}>تاریخ</Text>
        </View>
      </Row>
      {loading ? (
        <Loading color={purple}>
          <Text style={styles.loadingText}>در حال بارگزاری</Text>
        </Loading>
      ) : (
        <ScrollView>
          {customerTradeMoney.map(ctm => (
            <Row
              key={ctm.id}
              onPress={() =>
                navigateToPaidAndReceivedPage(ctm.buy, true, ctm.id)
              }>
              <View
                style={[
                  styles.cell,
                  styles.rightCell,
                  {
                    width: 33.3333 + "%",
                    backgroundColor: ctm.Received ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>
                  {ctm.isCustomer ? ctm.customerName : ctm.baseName}
                </Text>
              </View>
              <View
                style={[
                  styles.cell,
                  {
                    width: 33.3333 + "%",
                    backgroundColor: ctm.Received ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{ctm.price}</Text>
              </View>
              <View
                style={[
                  styles.cell,
                  styles.leftCell,
                  {
                    width: 33.3333 + "%",
                    backgroundColor: ctm.Received ? lightgreen : red,
                  },
                ]}>
                <Text style={styles.text}>{DateTime.toJalaali(ctm.date)}</Text>
              </View>
            </Row>
          ))}
        </ScrollView>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {},
  cell: {
    backgroundColor: dark,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  headingCell: {
    backgroundColor: purple,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "darkgrey",
  },
  rightCell: {
    borderTopRightRadius: styleConstants.borderRadius,
    borderBottomRightRadius: styleConstants.borderRadius,
  },
  leftCell: {
    borderTopLeftRadius: styleConstants.borderRadius,
    borderBottomLeftRadius: styleConstants.borderRadius,
  },
  text: {
    fontFamily: "Estedad-Thin",
    color: "white",
  },
  headingText: {
    fontFamily: "Estedad-Bold",
    color: "white",
  },
  addButtonsContainer: {
    position: "absolute",
    bottom: 0,
    height: 50,
    width: 100 + "%",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  addBuyButton: {
    backgroundColor: lightgreen,
  },
  addSellButton: {
    backgroundColor: purple,
    borderRadius: 50,
    position: "absolute",
    width: 60,
    height: 60,
    left: 10,
    bottom: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  addButtonText: {
    color: "white",
    fontFamily: "Estedad-Medium",
  },
  loadingText: {
    fontFamily: "Estedad-Medium",
    fontSize: 18,
  },
})

export default CustomerTradeMoney
