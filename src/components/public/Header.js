import React from 'react'
import { View, Text, StyleSheet } from 'react-native'

function Header (props) {
  return (
    <View style={ styles.container }>
      <Text style={ styles.text }>Menu</Text>
      <Text style={ styles.text }>Title</Text>
      <Text style={ styles.text }>Back</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: '#130f40',
  },
  hamburgerButton: {},
  backButton: {},
  text: {
    color: 'white',
  },
})

export default Header
