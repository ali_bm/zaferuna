/**
 * @format
 */
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import reducers from './src/store/reducers';
import {createStore} from 'redux';
import {Provider} from 'react-redux';

const store = createStore(reducers);

const Application = () => {
  return (
      <Provider store={ store }>
        <App />
      </Provider>
  );
};

AppRegistry.registerComponent(appName, () => Application);
